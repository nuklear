## About

A binding to the [nuklear] library.  Covers just enough of its API to
make all demo examples run.

## Docs

See [its wiki page].

[nuklear]: https://github.com/vurtun/nuklear
[its wiki page]: http://wiki.call-cc.org/eggref/4/nuklear
