(import scheme)
(import (chicken base))
(import (chicken bitwise))
(import (prefix glfw3 glfw:))
(import (prefix epoxy gl:))
(import (prefix nuklear-glfw3-opengl2 nk:))
(import (prefix nuklear nk:))

(define width 250)
(define height 150)

(glfw:init)
(glfw:make-window width height "Hello World")
(glfw:make-context-current (glfw:window))
(set!-values (width height) (glfw:get-window-size (glfw:window)))

(define ctx (nk:init (glfw:window)))
(nk:init-default-font ctx)

(define quit? #f)
(define show-greeting? #f)

(define ->flag bitwise-ior)

(let loop ()
  (when (and (not (glfw:window-should-close (glfw:window))) (not quit?))
    (glfw:poll-events)
    (nk:new-frame)

    (when (nk:window-begin ctx "Hello World!"
                           (nk:make-rect 10 10 192 100)
                           (->flag nk:window/border nk:window/no-scrollbar
                                   nk:window/movable))
      (nk:layout-row-dynamic ctx 30 2)
      (when (nk:button-label ctx "Click Me!")
        (print "Yay")
        (set! show-greeting? #t))
      (when (nk:button-label ctx "Quit")
        (set! quit? #t))

      (when show-greeting?
        (if (nk:popup-begin ctx nk:popup/static "Greeting"
                            (->flag nk:window/closable nk:window/border)
                            (nk:make-rect 15 50 200 150))
            (begin
              (nk:layout-row-dynamic ctx 25 1)
              (nk:label ctx "Hello World!" nk:text/centered)
              (nk:layout-row-dynamic ctx 25 1)
              (when (nk:button-label ctx "OK")
                (set! show-greeting? #f)
                (nk:popup-close ctx))
              (nk:popup-end ctx))
            (set! show-greeting? #f))))
    (nk:window-end ctx)

    (let-values (((width height) (glfw:get-window-size (glfw:window))))
      (gl:viewport 0 0 width height))
    (gl:clear gl:+color-buffer-bit+)
    (gl:clear-color (/ 28 255.0) (/ 48 255.0) (/ 62 255.0) 0.0)
    (nk:render nk:anti-aliasing/on)
    (glfw:swap-buffers (glfw:window))
    (loop)))

(nk:shutdown)
(glfw:terminate)
