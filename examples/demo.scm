(import scheme)
(import (chicken base))
(import (chicken bitwise))
;; (import (chicken pathname))
;; (import (chicken process-context))
(import (prefix glfw3 glfw:))
(import (prefix epoxy gl:))
(import (prefix nuklear-glfw3-opengl2 nk:))
(import (prefix nuklear nk:))

(define width 1200)
(define height 800)

(glfw:init)
(glfw:make-window width height "Demo" alpha-bits: 0)
(glfw:make-context-current (glfw:window))
(set!-values (width height) (glfw:get-window-size (glfw:window)))

(define ctx (nk:init (glfw:window)))
(nk:init-default-font ctx)
;; (define font-path (make-pathname (pathname-directory (program-name)) "DroidSans.ttf"))
;; (nk:init-file-font ctx font 15)

(define hard? #f)
(define property 20)
(define bg (nk:make-color 0.1 0.18 0.24 1.0))

(define ->flag bitwise-ior)
(define ->int inexact->exact)

(let loop ()
  (when (and (not (glfw:window-should-close (glfw:window))))
    (glfw:poll-events)
    (nk:new-frame)

    (when (nk:window-begin ctx "Demo"
                           (nk:make-rect 50 50 230 250)
                           (->flag nk:window/border nk:window/movable
                                   nk:window/scalable nk:window/minimizable
                                   nk:window/title))
      (nk:layout-row-static ctx 30 80 1)
      (when (nk:button-label ctx "button")
        (print "button pressed"))

      (nk:layout-row-dynamic ctx 30 2)
      (when (nk:option-label ctx "easy" (not hard?))
        (set! hard? #f))
      (when (nk:option-label ctx "hard" hard?)
        (set! hard? #t))

      (nk:layout-row-dynamic ctx 25 1)
      (set! property (nk:property-int ctx "Compression:" 0 property 100 10 1))

      (nk:layout-row-dynamic ctx 20 1)
      (nk:label ctx "background:" nk:text/left)
      (nk:layout-row-dynamic ctx 25 1)
      (when (nk:combo-begin-color ctx bg (nk:make-vec2 (->int (nk:widget-width ctx)) 400))
        (nk:layout-row-dynamic ctx 120 1)
        (set! bg (nk:color-picker ctx bg nk:color/rgba))
        (nk:layout-row-dynamic ctx 25 1)
        (set! (nk:color-r bg) (nk:property-float ctx "#R:" 0 (nk:color-r bg) 1.0 0.01 0.005))
        (set! (nk:color-g bg) (nk:property-float ctx "#G:" 0 (nk:color-g bg) 1.0 0.01 0.005))
        (set! (nk:color-b bg) (nk:property-float ctx "#B:" 0 (nk:color-b bg) 1.0 0.01 0.005))
        (set! (nk:color-a bg) (nk:property-float ctx "#A:" 0 (nk:color-a bg) 1.0 0.01 0.005))
        (nk:combo-end ctx)))
    (nk:window-end ctx)

    (let-values (((width height) (glfw:get-window-size (glfw:window))))
      (gl:viewport 0 0 width height))
    (gl:clear gl:+color-buffer-bit+)
    (gl:clear-color (nk:color-r bg) (nk:color-g bg) (nk:color-b bg) (nk:color-a bg))
    (nk:render nk:anti-aliasing/on)
    (glfw:swap-buffers (glfw:window))
    (loop)))

(nk:shutdown)
(glfw:terminate)
