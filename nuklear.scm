(cond-expand
 (keystate-based-input
  (foreign-declare "#define NK_KEYSTATE_BASED_INPUT"))
 (else))

#>
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT

#define NK_IMPLEMENTATION
#include "nuklear.h"
<#

;;; errors

(define (nuklear-error message location)
  (condition `(exn location ,location message ,message) '(nuklear)))

;;; enums

;; enum nk_anti_aliasing
(define anti-aliasing/off (foreign-value "NK_ANTI_ALIASING_OFF" int))
(define anti-aliasing/on (foreign-value "NK_ANTI_ALIASING_ON" int))

;; enum nk_button_behavior
(define button/default (foreign-value "NK_BUTTON_DEFAULT" int))
(define button/repeater (foreign-value "NK_BUTTON_REPEATER" int))

;; enum nk_modify
(define modify/fixed (foreign-value "NK_FIXED" int))
(define modify/modifiable (foreign-value "NK_MODIFIABLE" int))

;; enum nk_collapse_states
(define collapse/minimized (foreign-value "NK_MINIMIZED" int))
(define collapse/maximized (foreign-value "NK_MAXIMIZED" int))

;; enum nk_chart_type
(define chart/lines (foreign-value "NK_CHART_LINES" int))
(define chart/column (foreign-value "NK_CHART_COLUMN" int))

;; enum nk_chart_event
(define chart/hovering (foreign-value "NK_CHART_HOVERING" int))
(define chart/clicked (foreign-value "NK_CHART_CLICKED" int))

;; enum nk_color_format
(define color/rgb (foreign-value "NK_RGB" int))
(define color/rgba (foreign-value "NK_RGBA" int))

;; enum nk_popup_type
(define popup/static (foreign-value "NK_POPUP_STATIC" int))
(define popup/dynamic (foreign-value "NK_POPUP_DYNAMIC" int))

;; enum nk_layout_format
(define layout/dynamic (foreign-value "NK_DYNAMIC" int))
(define layout/static (foreign-value "NK_STATIC" int))

;; enum nk_tree_type
(define tree/node (foreign-value "NK_TREE_NODE" int))
(define tree/tab (foreign-value "NK_TREE_TAB" int))

;; enum nk_symbol_type
(define symbol/none (foreign-value "NK_SYMBOL_NONE" int))
(define symbol/x (foreign-value "NK_SYMBOL_X" int))
(define symbol/underscore (foreign-value "NK_SYMBOL_UNDERSCORE" int))
(define symbol/circle-solid (foreign-value "NK_SYMBOL_CIRCLE_SOLID" int))
(define symbol/circle-outline (foreign-value "NK_SYMBOL_CIRCLE_OUTLINE" int))
(define symbol/rect-solid (foreign-value "NK_SYMBOL_RECT_SOLID" int))
(define symbol/rect-outline (foreign-value "NK_SYMBOL_RECT_OUTLINE" int))
(define symbol/triangle-up (foreign-value "NK_SYMBOL_TRIANGLE_UP" int))
(define symbol/triangle-down (foreign-value "NK_SYMBOL_TRIANGLE_DOWN" int))
(define symbol/triangle-left (foreign-value "NK_SYMBOL_TRIANGLE_LEFT" int))
(define symbol/triangle-right (foreign-value "NK_SYMBOL_TRIANGLE_RIGHT" int))
(define symbol/plus (foreign-value "NK_SYMBOL_PLUS" int))
(define symbol/minus (foreign-value "NK_SYMBOL_MINUS" int))

;; enum nk_buttons
(define button/left (foreign-value "NK_BUTTON_LEFT" int))
(define button/middle (foreign-value "NK_BUTTON_MIDDLE" int))
(define button/right (foreign-value "NK_BUTTON_RIGHT" int))

;; enum nk_text_alignment
(define text/left (foreign-value "NK_TEXT_LEFT" int))
(define text/centered (foreign-value "NK_TEXT_CENTERED" int))
(define text/right (foreign-value "NK_TEXT_RIGHT" int))

;; enum nk_edit_types
(define edit/simple (foreign-value "NK_EDIT_SIMPLE" int))
(define edit/field (foreign-value "NK_EDIT_FIELD" int))
(define edit/box (foreign-value "NK_EDIT_BOX" int))
(define edit/editor (foreign-value "NK_EDIT_EDITOR" int))

;; enum nk_edit_flags
(define edit/default (foreign-value "NK_EDIT_DEFAULT" int))
(define edit/read-only (foreign-value "NK_EDIT_READ_ONLY" int))
(define edit/auto-select (foreign-value "NK_EDIT_AUTO_SELECT" int))
(define edit/sig-enter (foreign-value "NK_EDIT_SIG_ENTER" int))
(define edit/allow-tab (foreign-value "NK_EDIT_ALLOW_TAB" int))
(define edit/no-cursor (foreign-value "NK_EDIT_NO_CURSOR" int))
(define edit/selectable (foreign-value "NK_EDIT_SELECTABLE" int))
(define edit/clipboard (foreign-value "NK_EDIT_CLIPBOARD" int))
(define edit/ctrl-enter-newline (foreign-value "NK_EDIT_CTRL_ENTER_NEWLINE" int))
(define edit/no-horizontal-scroll (foreign-value "NK_EDIT_NO_HORIZONTAL_SCROLL" int))
(define edit/always-insert-mode (foreign-value "NK_EDIT_ALWAYS_INSERT_MODE" int))
(define edit/multiline (foreign-value "NK_EDIT_MULTILINE" int))
(define edit/goto-end-on-activate (foreign-value "NK_EDIT_GOTO_END_ON_ACTIVATE" int))

;; enum nk_edit_events
(define edit/active (foreign-value "NK_EDIT_ACTIVE" int))
(define edit/inactive (foreign-value "NK_EDIT_INACTIVE" int))
(define edit/activated (foreign-value "NK_EDIT_ACTIVATED" int))
(define edit/deactivated (foreign-value "NK_EDIT_DEACTIVATED" int))
(define edit/committed (foreign-value "NK_EDIT_COMMITED" int))

;; enum nk_panel_flags
(define window/border (foreign-value "NK_WINDOW_BORDER" int))
(define window/movable (foreign-value "NK_WINDOW_MOVABLE" int))
(define window/scalable (foreign-value "NK_WINDOW_SCALABLE" int))
(define window/closable (foreign-value "NK_WINDOW_CLOSABLE" int))
(define window/minimizable (foreign-value "NK_WINDOW_MINIMIZABLE" int))
(define window/no-scrollbar (foreign-value "NK_WINDOW_NO_SCROLLBAR" int))
(define window/title (foreign-value "NK_WINDOW_TITLE" int))
(define window/scroll-auto-hide (foreign-value "NK_WINDOW_SCROLL_AUTO_HIDE" int))
(define window/background (foreign-value "NK_WINDOW_BACKGROUND" int))
(define window/scale-left (foreign-value "NK_WINDOW_SCALE_LEFT" int))
(define window/no-input (foreign-value "NK_WINDOW_NO_INPUT" int))

;; enum nk_style_header_align
(define header/left (foreign-value "NK_HEADER_LEFT" int))
(define header/right (foreign-value "NK_HEADER_RIGHT" int))

;;; typedefs

(define-foreign-type nk_context* (nonnull-c-pointer (struct "nk_context")))
(define-foreign-type nk_command_buffer* (nonnull-c-pointer (struct "nk_command_buffer")))
(define-foreign-type nk_panel* (nonnull-c-pointer (struct "nk_panel")))
(define-foreign-type nk_input* (nonnull-c-pointer (struct "nk_input")))
(define-foreign-type nk_style* (nonnull-c-pointer (struct "nk_style")))
(define-foreign-type nk_mouse* (nonnull-c-pointer (struct "nk_mouse")))
(define-foreign-type nk_color* (nonnull-c-pointer (struct "nk_color")))
(define-foreign-type nk_vec2* (nonnull-c-pointer (struct "nk_vec2")))
(define-foreign-type nk_text_edit* (nonnull-c-pointer (struct "nk_text_edit")))
(define-foreign-type nk_flags unsigned-int32)
(define-foreign-type nk_flags* (nonnull-c-pointer nk_flags))
(define-foreign-type nk_size unsigned-long)
(define-foreign-type nk_size* (nonnull-c-pointer nk_size))
(define-foreign-type nk_rune unsigned-integer)
(define-foreign-type uint8* (nonnull-c-pointer unsigned-byte))
(define-foreign-type uint8 unsigned-byte)
(define-foreign-type float* (nonnull-c-pointer float))
(define-foreign-type int* (nonnull-c-pointer int))
(define-foreign-type bool* (nonnull-c-pointer bool))

(define-foreign-type nk_filter (function bool ((const nk_text_edit*) nk_rune)))

;;; struct member accessors

(define float-size (foreign-type-size float))

(define (context-style context*)
  ((foreign-lambda* nk_input* ((nk_context* ctx)) "C_return(&(ctx->style));") context*))

(define (style-window-header-align-set! style* header-flag)
  ((foreign-lambda* void ((nk_style* style) ((enum "nk_style_header_align") flag))
     "style->window.header.align = flag;")
   style* header-flag))

(define (context-input context*)
  ((foreign-lambda* nk_input* ((nk_context* ctx)) "C_return(&(ctx->input));") context*))

(define (input-mouse input*)
  ((foreign-lambda* nk_mouse* ((nk_input* input)) "C_return(&(input->mouse));") input*))

(define (mouse-position mouse*)
  (let-location ((a float)
                 (b float))
    ((foreign-lambda* void ((nk_mouse* mouse) (float* a) (float* b)) "*a = mouse->pos.x; *b = mouse->pos.y;") mouse* (location a) (location b))
    (make-vec2 a b)))

(define (mouse-delta mouse*)
  (let-location ((a float)
                 (b float))
    ((foreign-lambda* void ((nk_mouse* mouse) (float* a) (float* b)) "*a = mouse->delta.x; *b = mouse->delta.y;") mouse* (location a) (location b))
    (make-vec2 a b)))

(define (panel-bounds panel*)
  (let-location ((x float)
                 (y float)
                 (w float)
                 (h float))
    ((foreign-lambda* void ((nk_panel* panel) (float* x) (float* y) (float* w) (float* h)) "*x = panel->bounds.x; *y = panel->bounds.y; *w = panel->bounds.w; *h = panel->bounds.h;") panel* (location x) (location y) (location w) (location h))
    (make-rect x y w h)))

;;; stack-allocation helpers

(define-record rect x y w h)
(define rect-x (getter-with-setter rect-x rect-x-set!))
(define rect-y (getter-with-setter rect-y rect-y-set!))
(define rect-w (getter-with-setter rect-w rect-w-set!))
(define rect-h (getter-with-setter rect-h rect-h-set!))

(define-record color r g b a)
(define color-r (getter-with-setter color-r color-r-set!))
(define color-g (getter-with-setter color-g color-g-set!))
(define color-b (getter-with-setter color-b color-b-set!))
(define color-a (getter-with-setter color-a color-a-set!))

(define-record vec2 x y)
(define vec2-x (getter-with-setter vec2-x vec2-x-set!))
(define vec2-y (getter-with-setter vec2-y vec2-y-set!))

;;; foreign functions

;; window
(define nk_begin (foreign-lambda* bool ((nk_context* ctx) (nonnull-c-string title) (float x) (float y) (float w) (float h) (nk_flags flags)) "C_return(nk_begin(ctx, title, nk_rect(x, y, w, h), flags));"))
(define nk_end (foreign-lambda void "nk_end" nk_context*))

;; layout
(define nk_layout_row_dynamic (foreign-lambda void "nk_layout_row_dynamic" nk_context* float int))
(define nk_layout_row_static (foreign-lambda void "nk_layout_row_static" nk_context* float int int))
(define nk_layout_row_begin (foreign-lambda void "nk_layout_row_begin" nk_context* (enum "nk_layout_format") float int))
(define nk_layout_row_push (foreign-lambda void "nk_layout_row_push" nk_context* float))
(define nk_layout_row_end (foreign-lambda void "nk_layout_row_end" nk_context*))
(define nk_layout_row (foreign-lambda* void ((nk_context* ctx) ((enum "nk_layout_format") format) (float height) (int cols) (scheme-object data) (blob storage))
                        "int i; float *ratio = (float*) storage;"
                        "for (i = 0; i < cols; ++i)"
                        "  ratio[i] = (float) C_flonum_magnitude(C_block_item(data, i));"
                        "nk_layout_row(ctx, format, height, cols, ratio);"))

(define nk_layout_space_begin (foreign-lambda void "nk_layout_space_begin" nk_context* (enum "nk_layout_format") float int))
(define nk_layout_space_push (foreign-lambda* void ((nk_context* ctx) (float x) (float y) (float w) (float h)) "nk_layout_space_push(ctx, nk_rect(x, y, w, h));"))
(define nk_layout_space_end (foreign-lambda void "nk_layout_space_end" nk_context*))

;; group
(define nk_group_begin (foreign-lambda bool "nk_group_begin" nk_context* nonnull-c-string nk_flags))
(define nk_group_end (foreign-lambda void "nk_group_end" nk_context*))

;; tree
(define nk_tree_push_hashed (foreign-lambda bool "nk_tree_push_hashed" nk_context* (enum "nk_tree_type") nonnull-c-string (enum "nk_collapse_states") nonnull-c-string int int))
(define nk_tree_pop (foreign-lambda void "nk_tree_pop" nk_context*))
(define nk_tree_state_push (foreign-lambda bool "nk_tree_state_push" nk_context* (enum "nk_tree_type") nonnull-c-string (c-pointer (enum "nk_collapse_states"))))
(define nk_tree_state_pop (foreign-lambda void "nk_tree_state_pop" nk_context*))
(define nk_tree_element_push_hashed (foreign-lambda bool "nk_tree_element_push_hashed" nk_context* (enum "nk_tree_type") nonnull-c-string (enum "nk_collapse_states") bool* nonnull-c-string int int))
(define nk_tree_element_pop (foreign-lambda void "nk_tree_element_pop" nk_context*))

;; text/label
(define nk_text (foreign-lambda void "nk_text" nk_context* blob int nk_flags))
(define nk_label (foreign-lambda void "nk_label" nk_context* nonnull-c-string nk_flags))
(define nk_label_colored (foreign-lambda* void ((nk_context* ctx) (nonnull-c-string label) (nk_flags align) (int r) (int g) (int b) (int a)) "nk_label_colored(ctx, label, align, nk_rgba(r, g, b, a));"))
(define nk_label_wrap (foreign-lambda void "nk_label_wrap" nk_context* nonnull-c-string))

;; button
(define nk_button_label (foreign-lambda bool "nk_button_label" nk_context* nonnull-c-string))
(define nk_button_color (foreign-lambda* bool ((nk_context* ctx) (int r) (int g) (int b) (int a)) "C_return(nk_button_color(ctx, nk_rgba(r, g, b, a)));"))
(define nk_button_symbol (foreign-lambda bool "nk_button_symbol" nk_context* (enum "nk_symbol_type")))
(define nk_button_symbol_label (foreign-lambda bool "nk_button_symbol_label" nk_context* (enum "nk_symbol_type") nonnull-c-string nk_flags))
(define nk_button_set_behavior (foreign-lambda void "nk_button_set_behavior" nk_context* (enum "nk_button_behavior")))

;; checkbox
(define nk_check_label (foreign-lambda bool "nk_check_label" nk_context* nonnull-c-string bool))

;; option
(define nk_option_label (foreign-lambda bool "nk_option_label" nk_context* nonnull-c-string bool))

;; selectable
(define nk_select_label (foreign-lambda bool "nk_select_label" nk_context* nonnull-c-string nk_flags bool))
(define nk_select_symbol_label (foreign-lambda bool "nk_select_symbol_label" nk_context* (enum "nk_symbol_type") nonnull-c-string nk_flags bool))

;; slider
(define nk_slide_float (foreign-lambda float "nk_slide_float" nk_context* float float float float))
(define nk_slide_int (foreign-lambda int "nk_slide_int" nk_context* int int int int))

;; progressbar
(define nk_prog (foreign-lambda nk_size "nk_prog" nk_context* nk_size nk_size (enum "nk_modify")))

;; color picker
(define nk_color_picker (foreign-lambda* void ((nk_context* ctx) ((enum "nk_color_format") flag) (float* r) (float* g) (float* b) (float* a)) "struct nk_colorf c = {*r, *g, *b, *a}; c = nk_color_picker(ctx, c, flag); *r = c.r, *g = c.g, *b = c.b, *a = c.a;"))

;; property
(define nk_propertyf (foreign-lambda float "nk_propertyf" nk_context* nonnull-c-string float float float float float))
(define nk_propertyi (foreign-lambda int "nk_propertyi" nk_context* nonnull-c-string int int int int int))

;; textedit
(define nk_edit_string (foreign-lambda* c-string ((nk_context* ctx) (int* flag) (nk_flags flags) (nonnull-c-string text) (int len) (blob buffer) (int max) (nk_filter filter))
                         "strncpy(buffer, text, max+1);"
                         "*flag = nk_edit_string(ctx, flags, buffer, &len, max+1, filter);"
                         "buffer[len] = 0;"
                         "C_return(buffer);"))
(define filter-default (foreign-value "nk_filter_default" nk_filter))
(define filter-ascii (foreign-value "nk_filter_ascii" nk_filter))
(define filter-float (foreign-value "nk_filter_float" nk_filter))
(define filter-decimal (foreign-value "nk_filter_decimal" nk_filter))
(define filter-hex (foreign-value "nk_filter_hex" nk_filter))
(define filter-oct (foreign-value "nk_filter_oct" nk_filter))
(define filter-binary (foreign-value "nk_filter_binary" nk_filter))

;; chart
(define nk_chart_begin (foreign-lambda bool "nk_chart_begin" nk_context* (enum "nk_chart_type") int float float))
(define nk_chart_begin_colored (foreign-lambda* bool ((nk_context* ctx) ((enum "nk_chart_type") flag) (int r) (int g) (int b) (int a) (int r2) (int g2) (int b2) (int a2) (int count) (float max) (float min)) "C_return(nk_chart_begin_colored(ctx, flag, nk_rgba(r, g, b, a), nk_rgba(r2, g2, b2, a2), count, max, min));"))
(define nk_chart_push (foreign-lambda nk_flags "nk_chart_push" nk_context* float))
(define nk_chart_push_slot (foreign-lambda nk_flags "nk_chart_push_slot" nk_context* float int))
(define nk_chart_add_slot (foreign-lambda void "nk_chart_add_slot" nk_context* (enum "nk_chart_type") int float float))
(define nk_chart_add_slot_colored (foreign-lambda* void ((nk_context* ctx) ((enum "nk_chart_type") flag) (int r) (int g) (int b) (int a) (int r2) (int g2) (int b2) (int a2) (int count) (float min) (float max)) "nk_chart_add_slot_colored(ctx, flag, nk_rgba(r, g, b, a), nk_rgba(r2, g2, b2, a2), count, min, max);"))
(define nk_chart_end (foreign-lambda void "nk_chart_end" nk_context*))

;; popup
(define nk_popup_begin (foreign-lambda* bool ((nk_context* ctx) ((enum "nk_popup_type") type) (nonnull-c-string title) (nk_flags flags) (float x) (float y) (float w) (float h)) "C_return(nk_popup_begin(ctx, type, title, flags, nk_rect(x, y, w, h)));"))
(define nk_popup_close (foreign-lambda void "nk_popup_close" nk_context*))
(define nk_popup_end (foreign-lambda void "nk_popup_end" nk_context*))

;; combo box
(define nk_combo_string (foreign-lambda* int ((nk_context* ctx) (blob string) (int selected) (int count) (int height) (float x) (float y)) "C_return(nk_combo_string(ctx, string, selected, count, height, nk_vec2(x, y)));"))
(define nk_combo_begin_label (foreign-lambda* bool ((nk_context* ctx) (nonnull-c-string label) (float x) (float y)) "C_return(nk_combo_begin_label(ctx, label, nk_vec2(x, y)));"))
(define nk_combo_begin_color (foreign-lambda* bool ((nk_context* ctx) (int r) (int g) (int b) (int a) (float x) (float y)) "C_return(nk_combo_begin_color(ctx, nk_rgba(r, g, b, a), nk_vec2(x, y)));"))
(define nk_combo_close (foreign-lambda void "nk_combo_close" nk_context*))
(define nk_combo_end (foreign-lambda void "nk_combo_end" nk_context*))

;; contextual
(define nk_contextual_begin (foreign-lambda* bool ((nk_context* ctx) (nk_flags flags) (float a) (float b) (float x) (float y) (float w) (float h)) "C_return(nk_contextual_begin(ctx, flags, nk_vec2(a, b), nk_rect(x, y, w, h)));"))
(define nk_contextual_item_label (foreign-lambda bool "nk_contextual_item_label" nk_context* nonnull-c-string nk_flags))
(define nk_contextual_end (foreign-lambda void "nk_contextual_end" nk_context*))

;; tooltip
(define nk_tooltip (foreign-lambda void "nk_tooltip" nk_context* nonnull-c-string))

;; menu
(define nk_menubar_begin (foreign-lambda void "nk_menubar_begin" nk_context*))
(define nk_menubar_end (foreign-lambda void "nk_menubar_end" nk_context*))
(define nk_menu_begin_label (foreign-lambda* bool ((nk_context* ctx) (nonnull-c-string label) (nk_flags align) (float a) (float b)) "C_return(nk_menu_begin_label(ctx, label, align, nk_vec2(a, b)));"))
(define nk_menu_item_label (foreign-lambda bool "nk_menu_item_label" nk_context* nonnull-c-string nk_flags))
(define nk_menu_end (foreign-lambda void "nk_menu_end" nk_context*))

;; style

;; utils
(define nk_window_get_canvas (foreign-lambda nk_command_buffer* "nk_window_get_canvas" nk_context*))
(define nk_window_get_content_region (foreign-lambda* void ((nk_context* ctx) (float* x) (float* y) (float* w) (float* h)) "struct nk_rect rect = nk_window_get_content_region(ctx); *x = rect.x; *y = rect.y; *w = rect.w; *h = rect.h;"))
(define nk_window_get_panel (foreign-lambda nk_panel* "nk_window_get_panel" nk_context*))
(define nk_spacing (foreign-lambda void "nk_spacing" nk_context* int))

(define nk_widget_bounds (foreign-lambda* void ((nk_context* ctx) (float* x) (float* y) (float* w) (float* h)) "struct nk_rect rect = nk_widget_bounds(ctx); *x = rect.x; *y = rect.y; *w = rect.w; *h = rect.h;"))
(define nk_widget_width (foreign-lambda float "nk_widget_width" nk_context*))

(define nk_layout_space_bounds (foreign-lambda* void ((nk_context* ctx) (float* x) (float* y) (float* w) (float* h)) "struct nk_rect rect = nk_layout_space_bounds(ctx); *x = rect.x; *y = rect.y; *w = rect.w; *h = rect.h;"))
(define nk_layout_space_to_screen (foreign-lambda* void ((nk_context* ctx) (float* a1) (float* b1) (float a2) (float b2)) "struct nk_vec2 ret = nk_layout_space_to_screen(ctx, nk_vec2(a2, b2)); *a1 = ret.x; *b1 = ret.y;"))
(define nk_layout_space_rect_to_screen (foreign-lambda* void ((nk_context* ctx) (float* x1) (float* y1) (float* w1) (float* h1) (float x2) (float y2) (float w2) (float h2)) "struct nk_rect rect = nk_layout_space_rect_to_screen(ctx, nk_rect(x2, y2, w2, h2)); *x1 = rect.x; *y1 = rect.y; *w1 = rect.w; *h1 = rect.h;"))
(define nk_layout_space_rect_to_local (foreign-lambda* void ((nk_context* ctx) (float* x1) (float* y1) (float* w1) (float* h1) (float x2) (float y2) (float w2) (float h2)) "struct nk_rect rect = nk_layout_space_rect_to_local(ctx, nk_rect(x2, y2, w2, h2)); *x1 = rect.x; *y1 = rect.y; *w1 = rect.w; *h1 = rect.h;"))

;; drawing
(define nk_fill_circle (foreign-lambda* void ((nk_command_buffer* cbuf) (float x) (float y) (float w) (float h) (int r) (int g) (int b) (int a)) "nk_fill_circle(cbuf, nk_rect(x, y, w, h), nk_rgba(r, g, b, a));"))
(define nk_stroke_line (foreign-lambda* void ((nk_command_buffer* cbuf) (float x0) (float y0) (float x1) (float y1) (float line_thickness) (int r) (int g) (int b) (int a)) "nk_stroke_line(cbuf, x0, y0, x1, y1, line_thickness, nk_rgba(r, g, b, a));"))
(define nk_stroke_curve (foreign-lambda* void ((nk_command_buffer* cbuf) (float ax) (float ay) (float ctrl0x) (float ctrl0y) (float ctrl1x) (float ctrl1y) (float bx) (float by) (float line_thickness) (int r) (int g) (int b) (int a)) "nk_stroke_curve(cbuf, ax, ay, ctrl0x, ctrl0y, ctrl1x, ctrl1y, bx, by, line_thickness, nk_rgba(r, g, b, a));"))

;; input
(define nk_input_has_mouse_click_down_in_rect (foreign-lambda* bool ((nk_input* input) ((enum "nk_buttons") id) (float x) (float y) (float w) (float h) (bool down)) "C_return(nk_input_has_mouse_click_down_in_rect(input, id, nk_rect(x, y, w, h), down));"))
(define nk_input_mouse_clicked (foreign-lambda* bool ((nk_input* input) ((enum "nk_buttons") id) (float x) (float y) (float w) (float h)) "C_return(nk_input_mouse_clicked(input, id, nk_rect(x, y, w, h)));"))
(define nk_input_is_mouse_down (foreign-lambda bool "nk_input_is_mouse_down" nk_input* (enum "nk_buttons")))
(define nk_input_is_mouse_hovering_rect (foreign-lambda* bool ((nk_input* input) (float x) (float y) (float w) (float h)) "C_return(nk_input_is_mouse_hovering_rect(input, nk_rect(x, y, w, h)));"))
(define nk_input_is_mouse_prev_hovering_rect (foreign-lambda* bool ((nk_input* input) (float x) (float y) (float w) (float h)) "C_return(nk_input_is_mouse_prev_hovering_rect(input, nk_rect(x, y, w, h)));"))
(define nk_input_is_mouse_released (foreign-lambda bool "nk_input_is_mouse_released" nk_input* (enum "nk_keys")))

;;; API

;; widgets

(define (window-begin context* title rect panel-flag)
  (let ((x (rect-x rect))
        (y (rect-y rect))
        (w (rect-w rect))
        (h (rect-h rect)))
    (nk_begin context* title x y w h panel-flag)))

(define (window-end context*)
  (nk_end context*))

(define (layout-row-dynamic context* height columns)
  (nk_layout_row_dynamic context* height columns))

(define (layout-row-static context* height item-width columns)
  (nk_layout_row_static context* height item-width columns))

(define (layout-row-begin context* layout-format-flag row-height cols)
  (nk_layout_row_begin context* layout-format-flag row-height cols))

(define (layout-row-push context* value)
  (nk_layout_row_push context* value))

(define (layout-row-end context*)
  (nk_layout_row_end context*))

(define (layout-row context* layout-flag height ratios)
  ;; TODO: check composite type
  (let* ((columns (length ratios))
         (data (list->vector (map exact->inexact ratios)))
         (storage (make-blob (* columns float-size))))
    (nk_layout_row context* layout-flag height columns data storage)))

(define (layout-space-begin context* layout-format-flag height widget-count)
  (nk_layout_space_begin context* layout-format-flag height widget-count))

(define (layout-space-push context* bounds)
  (let ((x (rect-x bounds))
        (y (rect-y bounds))
        (w (rect-w bounds))
        (h (rect-h bounds)))
    (nk_layout_space_push context* x y w h)))

(define (layout-space-end context*)
  (nk_layout_space_end context*))

(define (group-begin context* title flags)
  (nk_group_begin context* title flags))

(define (group-end context*)
  (nk_group_end context*))

(define (tree-push-hashed context* tree-flag label collapse-flag identifier seed)
  (nk_tree_push_hashed context* tree-flag label collapse-flag identifier (string-length identifier) seed))

(define (tree-push context* tree-flag label collapse-flag)
  (nk_tree_push_hashed context* tree-flag label collapse-flag label (string-length label) 0))

(define (tree-pop context*)
  (nk_tree_pop context*))

(define (tree-state-push context* tree-flag label collapse-flag)
  (let-location ((collapse-flag int collapse-flag))
    (let ((ret (nk_tree_state_push context* tree-flag label (location collapse-flag))))
      (values ret collapse-flag))))

(define (tree-state-pop context*)
  (nk_tree_state_pop context*))

(define (tree-element-push-hashed context* tree-flag label collapse-flag selected? identifier seed)
  (let-location ((selected? bool selected?))
    (let ((ret (nk_tree_element_push_hashed context* tree-flag label collapse-flag (location selected?) identifier (string-length identifier) seed)))
      (values ret selected?))))

(define (tree-element-push context* tree-flag label collapse-flag selected?)
  (let-location ((selected? bool selected?))
    (let ((ret (nk_tree_element_push_hashed context* tree-flag label collapse-flag (location selected?) label (string-length label) 0)))
      (values ret selected?))))

(define (tree-element-pop context*)
  (nk_tree_element_pop context*))

(define (text context* string align-flag)
  (nk_text context* string (string-length string) align-flag))

(define (label context* text align-flag)
  (nk_label context* text align-flag))

(define (label-colored context* label align-flag color)
  (let ((r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color)))
    (nk_label_colored context* label align-flag r g b a)))

(define (label-wrap context* label)
  (nk_label_wrap context* label))

(define (button-label context* label)
  (nk_button_label context* label))

(define (button-color context* color)
  (let ((r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color)))
    (nk_button_color context* r g b a)))

(define (button-symbol context* symbol-type)
  (nk_button_symbol context* symbol-type))

(define (button-symbol-label context* symbol-type label align-flag)
  (nk_button_symbol_label context* symbol-type label align-flag))

(define (button-behavior-set! context* behavior-flag)
  (nk_button_set_behavior context* behavior-flag))

(define (check-label context* label active?)
  (nk_check_label context* label active?))

(define (option-label context* label active?)
  (nk_option_label context* label active?))

(define (selectable-label context* label align-flag active?)
  (nk_select_label context* label align-flag active?))

(define (selectable-symbol-label context* symbol-type label align-flag active?)
  (nk_select_symbol_label context* symbol-type label align-flag active?))

(define (slider-float context* min old-value max step)
  (nk_slide_float context* min old-value max step))

(define (slider-int context* min old-value max step)
  (nk_slide_int context* min old-value max step))

(define (progress context* old-value max modifiable?)
  (nk_prog context* old-value max modifiable?))

(define (color-picker context* color color-flag)
  (let-location ((r float (color-r color))
                 (g float (color-g color))
                 (b float (color-b color))
                 (a float (color-a color)))
    (nk_color_picker context* color-flag (location r) (location g) (location b) (location a))
    (make-color r g b a)))

(define (property-float context* label min value max step pixel-step)
  (nk_propertyf context* label min value max step pixel-step))

(define (property-int context* label min value max step pixel-step)
  (nk_propertyi context* label min value max step pixel-step))

(define (edit-string* context* edit-flags text max #!optional (filter-proc filter-default))
  (when (> (string-length text) max)
    (nuklear-error "String exceeds maximum length" 'edit-string))
  (let ((len (string-length text))
        (buffer (make-blob (add1 max))))
    (let-location ((edit-event int))
      (let ((text (nk_edit_string context* (location edit-event) edit-flags
                                  text len buffer max filter-proc)))
        (values text edit-event)))))

(define (edit-string context* edit-flags text max #!optional (filter-proc filter-default))
  (receive (text _edit-event) (edit-string* context* edit-flags text max filter-proc)
    text))

(define (chart-begin context* chart-flag count min-value max-value)
  (nk_chart_begin context* chart-flag count min-value max-value))

(define (chart-begin-colored context* chart-flag color highlight count min-value max-value)
  (let ((r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color))
        (r2 (color-r highlight))
        (g2 (color-g highlight))
        (b2 (color-b highlight))
        (a2 (color-a highlight)))
    (nk_chart_begin_colored context* chart-flag r g b a r2 g2 b2 a2 count min-value max-value)))

(define (chart-push context* value)
  (nk_chart_push context* value))

(define (chart-push-slot context* value slot)
  (nk_chart_push_slot context* value slot))

(define (chart-add-slot context* type-flag count min-value max-value)
  (nk_chart_add_slot context* type-flag count min-value max-value))

(define (chart-add-slot-colored context* type-flag color highlight count min-value max-value)
  (let ((r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color))
        (r2 (color-r highlight))
        (g2 (color-g highlight))
        (b2 (color-b highlight))
        (a2 (color-a highlight)))
    (nk_chart_add_slot_colored context* type-flag r g b a r2 g2 b2 a2 count min-value max-value)))

(define (chart-end context*)
  (nk_chart_end context*))

(define (popup-begin context* popup-flag title panel-flag rect)
  (let ((x (rect-x rect))
        (y (rect-y rect))
        (w (rect-w rect))
        (h (rect-h rect)))
    (nk_popup_begin context* popup-flag title panel-flag x y w h)))

(define (popup-close context*)
  (nk_popup_close context*))

(define (popup-end context*)
  (nk_popup_end context*))

(define (combo context* choices idx height size)
  ;; TODO: type check for list of strings
  (let ((x (vec2-x size))
        (y (vec2-y size))
        (string (string-intersperse choices "\x00"))
        (len (length choices)))
    (nk_combo_string context* (string->blob string) idx len height x y)))

(define (combo-begin-label context* label size)
  (let ((x (vec2-x size))
        (y (vec2-y size)))
    (nk_combo_begin_label context* label x y)))

(define (combo-begin-color context* color size)
  (let ((r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color))
        (x (vec2-x size))
        (y (vec2-y size)))
    (nk_combo_begin_color context* r g b a x y)))

(define (combo-close context*)
  (nk_combo_close context*))

(define (combo-end context*)
  (nk_combo_end context*))

(define (contextual-begin context* flags size bounds)
  (let ((a (vec2-x size))
        (b (vec2-y size))
        (x (rect-x bounds))
        (y (rect-y bounds))
        (w (rect-w bounds))
        (h (rect-h bounds)))
    (nk_contextual_begin context* flags a b x y w h)))

(define (contextual-item-label context* label align-flag)
  (nk_contextual_item_label context* label align-flag))

(define (contextual-end context*)
  (nk_contextual_end context*))

(define (tooltip context* label)
  (nk_tooltip context* label))

(define (menubar-begin context*)
  (nk_menubar_begin context*))

(define (menubar-end context*)
  (nk_menubar_end context*))

(define (menu-begin-label context* label align-flag size)
  (let ((a (vec2-x size))
        (b (vec2-y size)))
    (nk_menu_begin_label context* label align-flag a b)))

(define (menu-item-label context* label align-flag)
  (nk_menu_item_label context* label align-flag))

(define (menu-end context*)
  (nk_menu_end context*))

;; style

;; utils

(define (clamp lower x upper)
  (if (< x lower)
      lower
      (if (> x upper)
          upper
          x)))

(define (rgb->color r g b)
  (rgba->color r g b 255))

(define (rgba->color r g b a)
  (make-color (clamp 0 r 255) (clamp 0 g 255) (clamp 0 b 255) (clamp 0 a 255)))

(define (rgb-f->colorf r g b)
  (rgba-f->colorf r g b 1.0))

(define (rgba-f->colorf r g b a)
  (make-color (clamp 0 r 1.0) (clamp 0 g 1.0) (clamp 0 b 1.0) (clamp 0 a 1.0)))

(define (colorf->color color)
  (define (->int float) (inexact->exact (floor float)))
  (make-color (->int (* (clamp 0 (color-r color) 1.0) 255.0))
              (->int (* (clamp 0 (color-g color) 1.0) 255.0))
              (->int (* (clamp 0 (color-b color) 1.0) 255.0))
              (->int (* (clamp 0 (color-a color) 1.0) 255.0))))

;; TODO: this might be wrong
(define (colorf->hsva-f color)
  (let-location ((h float)
                 (s float)
                 (v float)
                 (a float))
    ((foreign-lambda* void ((float* h) (float* s) (float* v) (float* a) (float r) (float g) (float b) (float a2))
       "nk_color_hsva_f(h, s, v, a, nk_rgba_f(r, g, b, a2));")
     (location h) (location s) (location v) (location a)
     (color-r color) (color-g color) (color-b color) (color-a color))
    (list h s v a)))

(define (hsva-f->colorf h s v a)
  (let-location ((r float)
                 (g float)
                 (b float)
                 (a2 float))
    ((foreign-lambda* void ((float* r) (float* g) (float* b) (float* a2) (float h) (float s) (float v) (float a))
       "struct nk_colorf c = nk_hsva_colorf(h, s, v, a); *r = c.r; *g = c.g; *b = c.b; *a2 = c.a;")
     (location r) (location g) (location b) (location a2) h s v a)
    (make-color r g b a2)))

(define (spacing context* columns)
  (nk_spacing context* columns))

(define (widget-bounds context*)
  (let-location ((x float)
                 (y float)
                 (w float)
                 (h float))
    (nk_widget_bounds context* (location x) (location y) (location w) (location h))
    (make-rect x y w h)))

(define (widget-width context*)
  (nk_widget_width context*))

(define (window-panel context*)
  (nk_window_get_panel context*))

(define (window-canvas context*)
  (nk_window_get_canvas context*))

(define (window-content-region context*)
  (let-location ((x float)
                 (y float)
                 (w float)
                 (h float))
    (nk_window_get_content_region context* (location x) (location y) (location w) (location h))
    (make-rect x y w h)))

(define (layout-space-bounds context*)
  (let-location ((x float)
                 (y float)
                 (w float)
                 (h float))
    (nk_layout_space_bounds context* (location x) (location y) (location w) (location h))
    (make-rect x y w h)))

(define (layout-space-to-screen context* bounds)
  (let-location ((a float)
                 (b float))
    (nk_layout_space_to_screen context* (location a) (location b) (vec2-x bounds) (vec2-y bounds))
    (make-vec2 a b)))

(define (layout-space-rect-to-screen context* bounds)
  (let-location ((x float)
                 (y float)
                 (w float)
                 (h float))
    (nk_layout_space_rect_to_screen context* (location x) (location y) (location w) (location h)
                                    (rect-x bounds) (rect-y bounds) (rect-w bounds) (rect-h bounds))
    (make-rect x y w h)))

(define (layout-space-rect-to-local context* bounds)
  (let-location ((x float)
                 (y float)
                 (w float)
                 (h float))
    (nk_layout_space_rect_to_local context* (location x) (location y) (location w) (location h)
                                   (rect-x bounds) (rect-y bounds) (rect-w bounds) (rect-h bounds))
    (make-rect x y w h)))

;; drawing

(define (fill-circle command-buffer* bounds color)
  (let ((x (rect-x bounds))
        (y (rect-y bounds))
        (w (rect-w bounds))
        (h (rect-h bounds))
        (r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color)))
    (nk_fill_circle command-buffer* x y w h r g b a)))

(define (stroke-line command-buffer* x0 y0 x1 y1 line-thickness color)
  (let ((r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color)))
    (nk_stroke_line command-buffer* x0 y0 x1 y1 line-thickness r g b a)))

(define (stroke-curve command-buffer* ax ay ctrl0x ctrl0y ctrl1x ctrl1y bx by line-thickness color)
  (let ((r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color)))
    (nk_stroke_curve command-buffer* ax ay ctrl0x ctrl0y ctrl1x ctrl1y bx by
                     line-thickness r g b a)))

;; input

(define (input-mouse-click-down-in-rect? input* button-flag bounds down?)
  (let ((x (rect-x bounds))
        (y (rect-y bounds))
        (w (rect-w bounds))
        (h (rect-h bounds)))
    (nk_input_has_mouse_click_down_in_rect input* button-flag x y w h down?)))

(define (input-mouse-clicked? input* button-flag bounds)
  (let ((x (rect-x bounds))
        (y (rect-y bounds))
        (w (rect-w bounds))
        (h (rect-h bounds)))
    (nk_input_mouse_clicked input* button-flag x y w h)))

(define (input-mouse-down? input* button-flag)
  (nk_input_is_mouse_down input* button-flag))

(define (input-mouse-hovering-in-rect? input* bounds)
  (let ((x (rect-x bounds))
        (y (rect-y bounds))
        (w (rect-w bounds))
        (h (rect-h bounds)))
    (nk_input_is_mouse_hovering_rect input* x y w h)))

(define (input-mouse-previously-hovering-in-rect? input* bounds)
  (let ((x (rect-x bounds))
        (y (rect-y bounds))
        (w (rect-w bounds))
        (h (rect-h bounds)))
    (nk_input_is_mouse_prev_hovering_rect input* x y w h)))

(define (input-mouse-released? input* button-flag)
  (nk_input_is_mouse_released input* button-flag))
